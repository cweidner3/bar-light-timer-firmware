#include <xc.h>

#include <stdint.h>

/******************************************************************************/
/** Function Prototypes                                                      **/
/******************************************************************************/

void InitApp (void);
void InitTMR (void);

/******************************************************************************/
/** MACRO Definitions                                                        **/
/******************************************************************************/

#define SOFT_POWER_EN_PIN       (PORTAbits.RA2)
#define SOFT_POWER_EN_ACTIVE    (1)
#define SOFT_POWER_EN_DIR       (TRISAbits.TRISA2 = 0)

#define LED_OUT_PIN             (PORTAbits.RA5)
#define LED_OUT_ACTIVE          (1)
#define LED_OUT_DIR             (TRISAbits.TRISA5 = 0)


/******************************************************************************/
/** Main Function/Loop                                                       **/
/******************************************************************************/
volatile uint16_t g_counter = 0;
#define COUNTER_VALUE_TRIGGER   (30)

void main (void)
{
    InitApp();

    while(1)
    {
        if(g_counter >= COUNTER_VALUE_TRIGGER){
            // Turn off the LED Strip
            LED_OUT_PIN = !LED_OUT_ACTIVE;
            // Disable the power
            SOFT_POWER_EN_PIN = !SOFT_POWER_EN_ACTIVE;
        }
    } // while (1)
} // main (void)

/******************************************************************************/
/** Interrupt Service Routine                                                **/
/******************************************************************************/

// TODO Develop ISR code to add to g_counter

/******************************************************************************/
/** Function Definitions                                                     **/
/******************************************************************************/

void InitApp (void)
{
    // TODO Initialize periphreals

    // Disable the WDT
    WDTCONbits.SWDTEN = 0;
    
    // Take over control of the power switch
    SOFT_POWER_EN_DIR;
    SOFT_POWER_EN_PIN = SOFT_POWER_EN_ACTIVE;

    // Enable the LED output
    LED_OUT_DIR;
    LED_OUT_PIN = LED_OUT_ACTIVE;

    // Set-up the system clock frequency
    OSCCONbits.IRCF = 0x0;      // LFINTOSC source (31kHz)
    OSCCONbits.SCS = 0x2;       // Internal Oscillator block
    while(!OSCSTATbits.LFIOFR)
        ; // Wait for LFINTOSC to start up


    // Enable the Timer Module
    T2CONbits.T2OUTPS = 0x0;    // 1:1 postscaler
    T2CONbits.T2CKPS = 0x3;     // 1:64 prescaler
    PR2 = 249;                  // Period value 250 for 2 Hz Timer
    T2CONbits.TMR2ON = 1;       // Enabled
}
